variable "department" {
}

variable "application" {
}

variable "environment" {
  default = "dev"
}

variable "env" {
}

variable "ami_name_filter" {
}

variable "instance_type" {
}

variable "VPC" {
}

variable "subnet_tags" {
  description = "(optional) Tags used to identify your target subnets with selected VPC"

  default = {
    Type = "Public"
  }
}


variable "sized_block_device" {
  description = "(optional) Name of the block device that the launch template will size"
  default     = "/dev/sda1"
}

variable "tags" {
  description = "(optional) Additional tags to be applied to all resources"
  default     = {}
}

variable "ec2_disk_size" {
}

variable "min_size" {
}

variable "max_size" {
  default = "2"
}

variable "subnet1" {
}

variable "subnet2" {
}

########  Autoscaling policy Parameters ############################

variable "scaling_adjustment" {
}

variable "scaling_cooldown" {
}

## Cloudwatch variables ########################
variable "scaling_metric" {
}

variable "scaling_metric_period" {
}

variable "scaling_evaluation_periods" {
}

variable "scaling_high_bound" {
}

variable "scaling_low_bound" {
}


