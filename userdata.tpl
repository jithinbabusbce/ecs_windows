<powershell>
Import-Module ECSTools
New-Item -Type directory -Path $${env:ProgramFiles}\Amazon\ECS -Force 
New-Item -Type directory -Path $${env:ProgramData}\Amazon\ECS -Force
New-Item -Type directory -Path $${env:ProgramData}\Amazon\ECS\data -Force
$ecsExeDir = "$${env:ProgramFiles}\Amazon\ECS"
[Environment]::SetEnvironmentVariable("ECS_CLUSTER", "windows", "Machine")
[Environment]::SetEnvironmentVariable("ECS_LOGFILE", "$${env:ProgramData}\Amazon\ECS\log\ecs-agent.log", "Machine")
[Environment]::SetEnvironmentVariable("ECS_DATADIR", "$${env:ProgramData}\Amazon\ECS\data", "Machine")
$agentVersion = "latest"
$agentZipUri = "https://s3.amazonaws.com/amazon-ecs-agent/ecs-agent-windows-$agentVersion.zip"
$zipFile = "$${env:TEMP}\ecs-agent.zip"
Invoke-RestMethod -OutFile $$zipFile -Uri $agentZipUri
Expand-Archive -Path $zipFile -DestinationPath $ecsExeDir -Force
Set-Location $${ecsExeDir}
[bool]$EnableTaskIAMRoles = $false
if ($${EnableTaskIAMRoles}) {
  $HostSetupScript = Invoke-WebRequest https://raw.githubusercontent.com/aws/amazon-ecs-agent/master/misc/windows-deploy/hostsetup.ps1
  Invoke-Expression $$($HostSetupScript.Content)
}
New-Service -Name "AmazonECS" `
        -BinaryPathName "$ecsExeDir\amazon-ecs-agent.exe -windows-service" `
        -DisplayName "Amazon ECS" `
        -Description "Amazon ECS service runs the Amazon ECS agent" `
        -DependsOn Docker `
        -StartupType Manual
sc.exe failure AmazonECS reset=300 actions=restart/5000/restart/30000/restart/60000
sc.exe failureflag AmazonECS 1
Initialize-ECSAgent
Start-Service AmazonECS
</powershell>
<persist>true</persist>
