resource "aws_security_group" "this" {
  vpc_id      = "${var.VPC}"
  name        = "${var.env}-ecs-windows"
  description = "Security group that allows needed ports and all egress traffic"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  tags = {
    Name = "${var.env}-ecs-windows"
    env  = "${var.env}"
  }
}
