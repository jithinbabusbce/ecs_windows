data "aws_ami" "this" {
  owners      = ["amazon"]
  most_recent = true

  filter {
    name   = "name"
    values = ["${var.ami_name_filter}"]
  }
}


data "aws_vpc" "vpc" {
  id = "vpc-62779218"
}


data "aws_subnet_ids" "this" {
  vpc_id = "${data.aws_vpc.vpc.id}"

  tags = {
    Type = "Public"
  }
}

##https://github.com/jjno91/terraform-aws-ecs-windows/blob/master/main.tf
