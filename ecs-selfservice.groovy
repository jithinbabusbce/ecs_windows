pipeline {	
	parameters {
		choice(name: 'ENVIRONMENT', choices: ['dev','test'], description: 'Select which environment deployment variable file to use.')
        string(name: 'DEPARTMENT', defaultValue: 'agt-cloudteam-tools', description: 'Department')
        string(name: 'APPLICATION', defaultValue: 'cloudteam', description: 'Department')
        string(name: 'RESOURCE_CONTACT', defaultValue: '', description: 'Valid Email Address of person responsible for EC2 instances')
        string(name: 'AMI', defaultValue: 'Windows_Server-2016-English-Full-ECS_Optimized-2018.04.18', description: 'Used to lookup the AMI that will be used in the cluster launch template')
        choice(name: 'TYPE', choices: ['r4.xlarge','t2.medium','m4.large','m4.xlarge'], description: 'Select which instance type to use.')
	    choice(name: 'ADDITIONALSECURITYGROUPS', choices: ['sg-e919b7a2'], description: 'Select ADDITIONAL SECURITY GROUPS to use.')
	    string(name: 'VPC', defaultValue: 'vpc-0f5fa85f87bbd147f', description: 'VPC ID')
	    choice(name: 'DISK', choices: ['10','20','100'], description: 'Select Disk size for the EC2')
	    choice(name: 'MIN_SIZE', choices: ['1'], description: '(optional) Minimum node count for ASG')
 	    choice(name: 'MAX_SIZE', choices: ['2','3','4','5'], description: '(optional) Maximum node count for ASG')
 	    choice(name: 'SUBNET1', choices: ['subnet-0a2dcaeb03ff045f7'], description: 'Subnet')
 	    choice(name: 'SUBNET2', choices: ['subnet-0bcac69005343cd54'], description: 'Subnet')
 	    choice(name: 'SCALING_ADJUSTMENT', choices: ['1'], description: '(optional) https://www.terraform.io/docs/providers/aws/r/autoscaling_policy.html#scaling_adjustment')
        choice(name: 'SCALING_COOLDOWN', choices: ['180'], description: '(optional) https://www.terraform.io/docs/providers/aws/r/autoscaling_policy.html#cooldown')
        choice(name: 'SCALING_METRICS', choices: ['MemoryReservation'], description: '(optional) ECS cluster metric used to calculate scaling operations')
        choice(name: 'METRICS_PERIOD', choices: ['60'], description: '(optional) https://www.terraform.io/docs/providers/aws/r/cloudwatch_metric_alarm.html#period')
      	choice(name: 'EVALUATION_PERIOD', choices: ['2'], description: '(optional) https://www.terraform.io/docs/providers/aws/r/cloudwatch_metric_alarm.html#evaluation_periods')
 	    choice(name: 'HIGH_BOUND', choices: ['2'], description: '(optional) When scaling_metric is above this bound your cluster will scale up')
 	    choice(name: 'LOW_BOUND', choices: ['75'], description: '(optional) When scaling_metric is below this bound your cluster will scale down')
}

	environment {
		JENKINS_FILENAME           = "jenkins-${params.ENVIRONMENT}.tfvars"
		REGION                     =  'us-east-1'
		RESOURCE_PURPOSE           = 'SelfService'

	}

	agent {
		label 'master'
	}

	stages {
		stage('configure') {
			steps {
				script {
						writeJenkinsFile()			
				}
			}
		}
		stage('deploy ECS') {
			steps {
				script {
						runTerraform()
				}
			}
		}
		stage('preparing environment data') {
			steps {
				script {
					env.ENVIRONMENT_DETAILS = prepareEmail()
				}
			}
		}		
	}
	post {
		success {
			emailext body: "Self Service Environment Information\n${env.ENVIRONMENT_DETAILS}", 
				subject: "Self Service infrastructure - ${env.RESOURCE_PURPOSE}-${params.CHANNEL_NAME}-${BUILD_NUMBER}", to: "${params.RESOURCE_CONTACT}"
		}
	}
}

def writeJenkinsFile() {
	echo "-- Writing Jenkins File"
	sh """
		rm -rf terraform.tfstate
		rm -rf terraform.tfstate.backup
		rm -rf ${env.JENKINS_FILENAME}

	cat <<EOF >> ${env.JENKINS_FILENAME}
env                      = "${params.ENVIRONMENT}"
application              = "${params.APPLICATION}"
division                 = "${params.DIVISION}"
ami_name_filter          =  "${params.AMI}"
department               = "${params.DEPARTMENT}"
instance_type            = "${params.TYPE}"
VPC                      = "${params.VPC}"
ec2_disk_size            = "${params.DISK}"
min_size                 = "${params.MIN_SIZE}"
max_size                 = "${params.MAX_SIZE}"
subnet1                  = "${params.SUBNET1}"
subnet2                  = "${params.SUBNET2}"
scaling_adjustment       = "${params.SCALING_ADJUSTMENT}"
scaling_cooldown         = "${params.SCALING_COOLDOWN}"
scaling_metric           = "${params.SCALING_METRICS}"
scaling_metric_period    = "${params.METRICS_PERIOD}"
scaling_evaluation_periods = "${params.EVALUATION_PERIOD}"
scaling_high_bound       = "${params.HIGH_BOUND}"
scaling_low_bound        = "${params.LOW_BOUND}"


EOF

	"""
	return
}

def runTerraform() {
	echo "-- Running alb creation Terraform script"
	sh """
		git config --global http.sslVerify false
		terraform init 
		terraform get -update
		terraform validate -var-file=${env.JENKINS_FILENAME}
		terraform plan -var-file=${env.JENKINS_FILENAME}
		terraform apply -input=false -var-file=${env.JENKINS_FILENAME} -auto-approve
	"""
	return
}

def prepareEmail() {
	echo "-- Preparing Email"
	sh """
		if [ -f "./environment-information.txt" ]; then
			rm -rf ./environment-information.txt
		fi
		cat <<EOF >> ./environment-information.txt
--------------------------------------------------------------------------------					
Deploy Owner     : ${params.RESOURCE_CONTACT}
Resource Purpose : ${env.RESOURCE_PURPOSE}-${BUILD_NUMBER}
--------------------------------------------------------------------------------
EOF
	"""

	script {
		environmentInformation = sh(returnStdout: true, script: "cat ./environment-information.txt")
	}
	return environmentInformation	
}