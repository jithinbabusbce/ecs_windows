# Setup of the ECS Cluster
resource "aws_ecs_cluster" "cluster" {
  name = "windows"
}

# User-data
data "template_file" "user_data" {
  template = "${file("userdata.tpl")}"

}


resource "aws_launch_template" "this" {
  name_prefix            = "${lower(var.department)}-${var.application}-${lower(var.environment)}-"
  image_id               = "${data.aws_ami.this.image_id}"
  instance_type          = "${var.instance_type}"
  ebs_optimized          = true
  user_data              = "${base64encode(data.template_file.user_data.rendered)}"
  vpc_security_group_ids = ["${aws_security_group.this.id}"]
  tags                   = "${merge(map("Name", "${var.env}-ecs-windows"), var.tags)}"

  block_device_mappings {
    device_name = "${var.sized_block_device}"

    ebs {
      volume_size = "${var.ec2_disk_size}"
    }
  }

  iam_instance_profile {
    name = "${aws_iam_instance_profile.this.name}"
  }

  key_name = "redhat"

  monitoring {
    enabled = true
  }

  tag_specifications {
    resource_type = "volume"
    tags          = "${merge(map("Name", "${var.env}-ecs-windows"), var.tags)}"
  }

  tag_specifications {
    resource_type = "instance"
    tags          = "${merge(map("Name", "${var.env}-ecs-windows"), var.tags)}"
  }
}

